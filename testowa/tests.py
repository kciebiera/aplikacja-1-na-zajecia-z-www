from django.test import TestCase
from testowa.models import Pozycja

class SimpleTestCase(TestCase):
    def setUp(self):
        pass 

    def test_opis(self):
        p1 = Pozycja.objects.get(pk=1)
        self.assertEqual(p1.opis, 'Pozycja 1')

    def test_liczba(self):
        self.assertEqual(Pozycja.objects.count(), 6)
        p = Pozycja(opis = 'Pozycja 7')
        self.assertEqual(Pozycja.objects.count(), 6)
        p.save()
        self.assertEqual(Pozycja.objects.count(), 7)
        p.delete()
        self.assertEqual(Pozycja.objects.count(), 6)

from django.shortcuts import render, redirect, get_object_or_404
from testowa.models import Pozycja
from django.contrib.auth import logout, authenticate, login
from django.contrib import messages

def glowna(request):
    return render(request, 'glowna.html')

def lista(request):
    pozycje = Pozycja.objects.all()
    slownik = {'poz': pozycje}
    return render(request, 'lista.html', slownik)

def szczegoly(request, pk):
    o = get_object_or_404(Pozycja, pk=int(pk))
    slownik = {'object': o}
    return render(request, 'szczegoly.html', slownik)

def szukaj(request):
    zapytanie = request.GET.get('zapytanie', '')
    pozycje = Pozycja.objects.filter(opis__contains=zapytanie)
    slownik = {'poz': pozycje, 'zapytanie': zapytanie}
    return render(request, 'szukaj.html', slownik)

def lista_zalogowana(request):
    if request.user.is_authenticated():
        return lista(request)
    else:
        return redirect('glowna')

def wyloguj(request):
    logout(request)
    return redirect('glowna')

def zaloguj(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
        else:
            messages.error(request, 'Konto zablokowane')
    else:
        messages.error(request, 'Zle haslo')
    return redirect('glowna')

from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView
from testowa.models import Pozycja

urlpatterns = patterns('',
    url(r'^$', 'testowa.views.glowna', name='glowna'),
    url(r'^glowna_klasowa$', TemplateView.as_view(template_name="glowna.html"),
        name='glowna_klasowa'),
    url(r'^lista$', 'testowa.views.lista', name='lista'),
    url(r'^lista_klasowa$', ListView.as_view(model=Pozycja), name='lista_klasowa'),
    url(r'^lista_zalogowana$', 'testowa.views.lista_zalogowana', 
        name='lista_zalogowana'),
    url(r'^login$', 'testowa.views.zaloguj', 
        name='login'),
    url(r'^logout$', 'testowa.views.wyloguj', name='logout'),
    url(r'^szukaj$', 'testowa.views.szukaj', name='szukaj'),
    url(r'^szczegoly/(?P<pk>\d+)$', 'testowa.views.szczegoly', name='szczegoly'),
)

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'testowa.views.glowna', name='poczatek'),
    url(r'^testowa/', include('testowa.urls')),
)
